<?php

namespace App\Controller;

use Framework\Core\Controller;
use Framework\Core\HTTP\Response;
use Framework\Core\Renderer;

class DefaultController extends Controller
{
    public function Home()
    {
        return $this->renderer->render("default.html.twig");
    }

    public function Error_404()
    {
        return $this->renderer->render("404.html.twig", 404);
    }

}   