<?php

namespace Framework\Core;

/**
 * a class for parsing a config file with parameters stored in an enviroment file
 * enviroment file must named like "name_of_enviroment.env"
 * The choosen enviroment must be set in config file
 */
class Config
{
    /**
     * config instance
     */
    private $config;

    /**
     * parameters from an enviroment file
     */
    private $parameters;

    /**
     * name of enviroment, by default its 'dev'
     */
    private $enviroment = "dev";

    /**
     * name of config file
     */
    private $filename;

    /**
     * constructor
     * @param (filename) filename of config file
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
        $this->loadConfigFile($filename);
        $this->enviroment = self::getValue("GENERAL", "enviroment");
        $this->loadEnviroment($this->enviroment);
    }

    /**
     * parse config file and store it in $this->config
     * @param (path) filename of config file
     */
    private function loadConfigFile($path)
    {
        if(file_exists(__CONFIG_PATH__ . $path) == false)
        {
            throw new \Framework\Exceptions\ConfigFileNotFoundException();
        }

        $this->config = parse_ini_file(__CONFIG_PATH__ . $path, TRUE);
    }

    /**
     * parse enviroment file and store it in $this->parameters
     * @param (name) name of enviroment
     */
    private function loadEnviroment($name)
    {
        $this->parameters = parse_ini_file(__CONFIG_PATH__ . $name . ".env");
    }

    /**
     * get value from a parameter key
     * when key not found, returns false
     * @param (name) key of parameter
     * @return (String) parameter if it exists, otherwise false
     */
    private function getParameter($name)
    {
        $parameter = $this->parameters[$name];

        if($parameter == false)
        {
            return false;
        }

        return $parameter;
    }

    /**
     * get name of config file
     */
    public function getFilename() 
    {
        return $this->filename;
    } 

    /**
     * get value of config file
     * @param (section) section of Key
     * @param (key) key
     * @return (String) value 
     */
    public function getValue($section, $key)
    {
        $value = $this->config[$section][$key];
        $matches = array();

        if(preg_match("/(?<=%).+(?=%)/", $value, $matches))
        {
            $value = $this->getParameter($matches[0]);
        }

        return $value;
    }
}