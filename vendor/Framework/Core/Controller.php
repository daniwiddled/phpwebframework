<?php

namespace Framework\Core;

class Controller
{
    /**
     * renderer to render twig templates
     */
    protected $renderer;

    /**
     * constructor
     * initialize renderer
     */
    public function __construct()
    {
        $this->renderer = new Renderer(\Framework::getConfig()->getValue("RENDERER","cache"), "app/Views/");
    }
}