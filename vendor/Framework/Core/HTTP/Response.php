<?php

namespace Framework\Core\HTTP;

/**
 * This class represents a HTTP 1.1 Response
 */
class Response
{
    /**
     * the content of the HTTP Response
     */
    private $content;

    /**
     * the HTTP status code of the response
     */
    private $statusCode;

    /**
     * constructor
     * @param (content) HTTP Response content
     * @param (statusCode) HTTP Response Status Code (by default 200)
     */
    public function __construct($content, $statusCode = 200)
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
    }

    /**
     * @return (string) content of HTTP response
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return (integer) status code of HTTP response
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}