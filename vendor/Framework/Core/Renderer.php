<?php

namespace Framework\Core;

use Framework\Core\HTTP\Response;

/**
 * class represents the twig renderer
 */
class Renderer
{
    private $twig;

    /**
     * constructor
     * @param (cache) when cache should be disabled, this = false, otherwise this is path to cache folder
     * @param (path) base path to twig templates
     */
    public function __construct($cache, $path)
    {
        if($cache !== false)
        {
            $cache = "../" . $cache;
        }

        $loader = new \Twig_Loader_Filesystem("../" . $path);
        $this->twig = new \Twig_Environment($loader, array(
            'cache' => $cache,
        ));
    }

    /**
     * renders a twig template
     * @param (template) name of template
     * @param (code) HTTP status code
     * @param (arguments) twig arguments (by default its [])
     */
    public function render($template, $code = 200, $arguments = array())
    {
        return new Response($this->twig->render($template, $arguments), $code);
    }
}