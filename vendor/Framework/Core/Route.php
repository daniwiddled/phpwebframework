<?php

namespace Framework\Core;

/**
 * represents a route
 */
class Route
{
    /**
     * controller called for route
     */
    public $controller;

    /**
     * function called for route
     */
    public $function;

    /**
     * HTTP parameters
     */
    public $parameters;

    /**
     * 
     */
    function __construct($path, $parameters = array())
    {
        if (strpos($path, ':') !== false) 
        {
            $split = explode(":", $path);
            $this->controller = $split[0];
            $this->function = $split[1];
        }
        else
        {
            $this->controller = $path;
            $this->function = "index";
        }
        
        $this->parameters = $parameters;
    }
}