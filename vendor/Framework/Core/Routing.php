<?php

namespace Framework\Core;

use Framework\Core\HTTP\Response;
use Framework\Core\Route;

/**
 * this class matches the current url to a defined route and call a specified function of a specified controller 
 */
class Routing
{
    private $routes = array();
    private $config;

    /**
     * constructor
     * 
     * @param (Framework\Core\Config) config 
     */
    public function __construct($config)
    {
        self::loadRoutes("routes.xml");
        $this->config = $config;
    }

    /**
     * routes the current url to specified method
     */
    public function dispatch() {

        // Prepare input
        $requestUrl = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
        $requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

        // Remove first and last slash
        $requestUrl = substr($requestUrl, 0, 1) == "/" ? substr($requestUrl, 1) : $requestUrl;
        $requestUrl = $requestUrl == "" ? "/" : $requestUrl;
        
        // Loop through all routes
        foreach ($this->routes as $route)
        {
            
            // Check request method
            if ($requestMethod == $route['method'] || $route["method"] == "ANY"){
                
                // Check if valid
                $regex = preg_match(self::convertRoute($route['url']), $requestUrl, $matches);
                if ($regex) {
                    
                    // Get parameters
                    preg_match_all('!\[(.+?)\]!', $route['url'], $params);

                    // Create array
                    $parameters = array();
                    foreach ($params[1] as $param) {
                        array_push($parameters, $matches[$param]);
                    }
                   
                    return new Route($route['function'], $parameters);
                }

            }
        }

        return new Route($this->config->getValue("ROUTING", "404_route"), array("code" => 404));
        
    }

    /**
     * reads 'routes.xml' file  
     */
    private function loadRoutes($filename)
    {  
       $routes = \simplexml_load_file(__CONFIG_PATH__ . $filename);
       foreach($routes as $route)
       {
            array_push($this->routes, array("name" => $route->attributes()["name"][0], "method" => $route->attributes()["method"], "url" =>$route->attributes()["url"], "function" => $route[0]));
       }
    }

    // Route => string in routes.xml
    private static function convertRoute($route) 
    {
        // $path => /user/[id]/[username]
        $route = substr($route, 0, 1) == "/" && $route != "/" ? substr($route, 1) : $route;
        // $path = user/[id]/[username]

        $regex = preg_replace('!\/!', '\/', $route);
        // $path = user\/[id]\/[username]

        $regex = preg_replace('!\[(.+?)\]!', '(?P<${1}>[^\/]+)', $regex);
        // $regex => user\/(?P<id>[^\/]+)\/(?P<username>[^\/]+)
        
        return '!^'.$regex.'$!';
        // =>  !^user\/(?P<id>[^\/]+)\/(?P<username>[^\/]+)$!
    }

}