<?php

namespace Framework\Exceptions;

class ConfigFileNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct(__CONFIG_PATH__ . "config.ini not found");
    }
}