<?php

namespace Framework\Exceptions;

class NoRouteFoundException extends \Exception
{
    public function __construct($url, $method)
    {
        parent::__construct("No route found for '" . $method . ":" . $url . "'");
    }
}