<?php

if(file_exists(__DIR__ . "/./../autoload.php"))
{
    require_once(__DIR__ . "/./../autoload.php");
}
else
{
    echo "<h1>ERROR</h1><br>Composer PSR-4 Autoloader not found! You have to make a composer install!";
    die();
}

class Framework
{
    private static $_config;
    private $exceptionRenderer;

    public function __construct()
    {
        
        define("__CONFIG_PATH__", __DIR__ . "/../../config/");
        $this->exceptionRenderer = new Framework\Core\Renderer(false, "vendor/Framework/Templates/Views/");


        try
        {
            $config = new Framework\Core\Config("config.ini");
            self::$_config = $config;
        }
        catch(Framework\Exceptions\ConfigFileNotFoundException $e)
        {
             $this->showException("Config File 'config.ini' not found!");
        }

        // global defines
        define("__BASE_PATH__", $config->getValue("GENERAL", "base_path"));

        // initialization
        $routing = new Framework\Core\Routing($config);
        $renderer = new Framework\Core\Renderer($config->getValue("RENDERER","cache"), "app/Views/");
 
        $route = $routing->dispatch();
        $controllerPath = $functionString = "App\\Controller\\" . $route->controller;
        $controller = new $controllerPath();

        $response = call_user_func_array(array($controller, $route->function), $route->parameters);

        if(gettype($response) === "object" && get_class($response) === "Framework\Core\HTTP\Response")
        {
            http_response_code($response->getStatusCode());
            echo $response->getContent();
        }
        else
        {
            $this->showException($route->controller . ":" .  $route->function . "  must return a Response or render a template");
        }  

    }

    public static function getConfig()
    {
        return self::$_config;
    }

    public function showException($message)
    {
        echo $this->exceptionRenderer->render("ExceptionView.html.twig", 500,  array("EXCEPTION_MESSAGE" => $message))->getContent();
        exit();
    }
}